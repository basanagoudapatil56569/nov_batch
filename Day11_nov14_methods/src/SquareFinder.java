import java.util.Scanner;

public class SquareFinder {

	static void square(int number) {
		System.out.println(number * number);
	}

	public static void main(String[] args) {
		// call the static square method and pass value as 5
		// hint: use class name and .(dot) operator for calling methods

		Scanner s = new Scanner(System.in);
		System.out.println("Enter the value of n");
		int n = s.nextInt();
		SquareFinder.square(n);
		s.close();
	}

}
