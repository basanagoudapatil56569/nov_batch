
public class TrickyLoop {
	public static void main(String[] args) {
		// if you use semicolon next to the for loop
		// it is like empty for loop
		for (int i = 1; i <= 5; i++);
		{
			System.out.println("Hello");
		}
	}
}
