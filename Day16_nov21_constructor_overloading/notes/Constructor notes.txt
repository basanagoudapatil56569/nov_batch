Writing more than one constructor in a class is known as constructor
overloading

Rules:
1. There must be change in the parameter list
	a. Either in the no. of parameters
	b. or in the datatype of parameters
	c. or in the sequence of parameters


1. can we write our own zero param constructor in a class?

Ans: yes

___________________________
2. When we write our own parameterized constructor 
will the compiler give zero param constructor?

Ans: no it will not provide, we have to write our own if required.

