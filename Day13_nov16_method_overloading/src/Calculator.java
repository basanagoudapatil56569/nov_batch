
public class Calculator {

	static int add(int a, int b) {
		return a + b;
	}

	static int add(int a, int b, int c) {
		return a + b + c;
	}

	static double add(double a, double b) {
		return a + b;
	}

	// main| ctrl+space will give main method
	public static void main(String[] args) {
		int result = Calculator.add(10, 20);
		System.out.println(result); // 30
		
		int sum = Calculator.add(10, 20, 30);
		System.out.println(sum); // 60
	}

}
