package com.trai.simexample;

public class RunnerSim {

	public static void main(String[] args) {
		AbstractSim a = new JioSim(); // upcasting
		a.call();
		a.sms();
		a.sos();
		
		// to access the sub class specific method
		// we have to downcast
		// to avoid ClassCastException we use instanceof operator
		if (a instanceof JioSim j) {
			j.jioTV();
		}
	}
}
