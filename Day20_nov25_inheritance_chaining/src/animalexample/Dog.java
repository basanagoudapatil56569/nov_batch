package animalexample;

public class Dog extends Animal {

	void bark() {
		System.out.println("Dog barks");
	}

//	public Dog() {
//		super();
//	}

	public static void main(String[] args) {
		Dog d = new Dog();
		System.out.println(d.color); // null
		System.out.println(d.breed); // null
		d.eat();
		d.bark();
		d.bark();
	}
}
