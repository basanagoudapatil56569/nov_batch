package vehicleexample;

public class Car extends Vehicle {
	boolean isAutomatic;

	void cruizeControl() {
		System.out.println("Car is on auto pilot mode");
	}

	@Override
	public String toString() {
		return "Car [color=" + color + ", brand=" + brand + ", price=" + price + ", isAutomatic=" + isAutomatic + "]";
	}
	
}
